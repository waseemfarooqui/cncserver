package com.air.cncserver.repositories;

import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.web.multipart.MultipartFile;

public interface StorageServiceInterface {

    ObjectNode store(MultipartFile file, String hash, String directory);

}
