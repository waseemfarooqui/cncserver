package com.air.cncserver.repositories;

import com.air.cncserver.models.Infections;
import org.springframework.data.jpa.repository.JpaRepository;

public interface InfectionRepository extends JpaRepository<Infections, Long> {

    Infections findByPcSignatureHash(String hash);
}
