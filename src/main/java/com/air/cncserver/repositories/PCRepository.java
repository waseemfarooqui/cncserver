package com.air.cncserver.repositories;

import com.air.cncserver.models.PCs;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PCRepository extends JpaRepository<PCs, Long> {

    public PCs findById(String id);
}
