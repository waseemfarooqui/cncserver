package com.air.cncserver.repositories;

import com.air.cncserver.models.Infections;
import com.air.cncserver.models.Tasks;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface TaskRepository extends JpaRepository<Tasks, Long> {
    public Set<Tasks> findTasksByInfectionsAndAndCompleted(Infections infection, Boolean completed);
}
