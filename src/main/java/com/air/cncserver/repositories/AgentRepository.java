package com.air.cncserver.repositories;

import com.air.cncserver.models.Agents;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AgentRepository extends JpaRepository<Agents, Long> {

    Agents findAgentsByUserCode(Long code);

    Agents findAgentsByUserName(String userName);
}
