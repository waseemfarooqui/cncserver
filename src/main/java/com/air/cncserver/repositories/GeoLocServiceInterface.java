package com.air.cncserver.repositories;

import com.air.cncserver.models.GeoLocations;
import com.maxmind.geoip2.exception.GeoIp2Exception;

import java.io.IOException;

public interface GeoLocServiceInterface {

    GeoLocations getLocation(String ip)  throws IOException, GeoIp2Exception;
}
