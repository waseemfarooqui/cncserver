package com.air.cncserver.repositories;

import com.air.cncserver.models.AgentGroup;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<AgentGroup, Long> {

    public AgentGroup findByName(String name);
}
