package com.air.cncserver.repositories;

import com.air.cncserver.models.PCs;
import com.air.cncserver.models.Statuses;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StatusesRepository extends JpaRepository<Statuses, Long> {

    public Statuses findByPc(PCs pc);
}
