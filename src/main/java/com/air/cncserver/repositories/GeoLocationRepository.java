package com.air.cncserver.repositories;

import com.air.cncserver.models.GeoLocations;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GeoLocationRepository extends JpaRepository<GeoLocations, Long> {

    public GeoLocations getByIpAddress(String ip);
}
