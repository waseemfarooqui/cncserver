package com.air.cncserver.models;

import java.io.Serializable;

public class TaskResponseModel implements Serializable {

    private Long id;
    private String name;
    private String command;
    private String pcSignatureHash;

    public TaskResponseModel() {
    }

    public TaskResponseModel(Long id, String name, String command) {
        this.id = id;
        this.name = name;
        this.command = command;
    }

    public TaskResponseModel(String name, String command) {
        this.name = name;
        this.command = command;
    }

    public TaskResponseModel(String name, String command, String pcSignatureHash) {
        this.name = name;
        this.command = command;
        this.pcSignatureHash = pcSignatureHash;
    }


    public TaskResponseModel(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getPcSignatureHash() {
        return pcSignatureHash;
    }

    public void setPcSignatureHash(String pcSignatureHash) {
        this.pcSignatureHash = pcSignatureHash;
    }
}
