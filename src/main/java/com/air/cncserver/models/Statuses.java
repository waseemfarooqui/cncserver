/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.air.cncserver.models;

import javax.persistence.*;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 28-11-2018
 * <pattern>
 * The Model class is basically an Entity for the database that would used to store the PC status and save the change if any of it is made.
 */
@Entity
@Table(name = "statuses")
public class Statuses {
    private Long id;
    private String lastSeenDate;
    private String firstSeenDate;
    private String changedValue;
    private String changedOn;
    private String previousValue;
    private String updatedValue;

    private PCs pc;

    public Statuses() {
    }

    public Statuses(String lastSeenDate, String changedValue, String changedOn) {
        this.lastSeenDate = lastSeenDate;
        this.changedValue = changedValue;
        this.changedOn = changedOn;
    }

    public Statuses(String lastSeenDate, String firstSeenDate, PCs pc) {
        this.lastSeenDate = lastSeenDate;
        this.firstSeenDate = firstSeenDate;
        this.pc = pc;
    }

    public Statuses(String changedValue, String changedOn, String lastSeenDate, String previousValue, String updatedValue, PCs pc) {
        this.changedValue = changedValue;
        this.changedOn = changedOn;
        this.lastSeenDate = lastSeenDate;
        this.previousValue = previousValue;
        this.updatedValue = updatedValue;
        this.pc = pc;
    }

    public Statuses(String lastSeenDate, PCs pc) {
        this.lastSeenDate = lastSeenDate;
        this.pc = pc;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    @NotNull
//    @Enumerated(EnumType.STRING)
//    public StatusTypes getType() {
//        return type;
//    }
//
//    public void setType(StatusTypes type) {
//        this.type = type;
//    }

    public String getLastSeenDate() {
        return lastSeenDate;
    }

    public void setLastSeenDate(String lastSeenDate) {
        this.lastSeenDate = lastSeenDate;
    }

    public String getFirstSeenDate() {
        return firstSeenDate;
    }

    public void setFirstSeenDate(String firstSeenDate) {
        this.firstSeenDate = firstSeenDate;
    }

    public String getChangedValue() {
        return changedValue;
    }

    public void setChangedValue(String changedValue) {
        this.changedValue = changedValue;
    }

    public String getChangedOn() {
        return changedOn;
    }

    public void setChangedOn(String changedOn) {
        this.changedOn = changedOn;
    }

    public String getPreviousValue() {
        return previousValue;
    }

    public void setPreviousValue(String previousValue) {
        this.previousValue = previousValue;
    }

    public String getUpdatedValue() {
        return updatedValue;
    }

    public void setUpdatedValue(String updatedValue) {
        this.updatedValue = updatedValue;
    }

    @ManyToOne
    @JoinColumn(name = "pcs_id", referencedColumnName = "id")
    public PCs getPc() {
        return pc;
    }

    public void setPc(PCs pc) {
        this.pc = pc;
    }
}
