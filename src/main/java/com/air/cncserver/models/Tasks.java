/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.air.cncserver.models;

import com.air.cncserver.enumerations.TaskNames;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 28-11-2018
 * <pattern>
 * The Model class is basically an Entity for the database that would used to store the Tasks for the infection.
 */
@Entity
@Table(name = "tasks")
public class Tasks {

    private Long id;
    private TaskNames name;
    private String createdDate;
    private boolean completed;
    private String command;
    private String response;
    private Set<Infections> infections;

    public Tasks() {
    }

    public Tasks(TaskNames name, String command) {
        this.name = name;
        this.command = command;
    }

    public Tasks(Long id, String response) {
        this.id = id;
        this.response = response;
    }

    public Tasks(String command, Set<Infections> infections) {
        this.command = command;
        this.infections = infections;
    }

    public Tasks(TaskNames name, Set<Infections> infections) {
        this.name = name;
        this.infections = infections;
    }

    public Tasks(TaskNames name, String createdDate, boolean completed, String command, String response) {
        this.name = name;
        this.createdDate = createdDate;
        this.completed = completed;
        this.command = command;
        this.response = response;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Enumerated(EnumType.STRING)
    public TaskNames getName() {
        return name;
    }

    public void setName(TaskNames name) {
        this.name = name;
    }

    @NotNull
    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "tasks_infections", joinColumns = @JoinColumn(name = "tasks_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "infections_pc_signature_hash", referencedColumnName = "pcSignatureHash"))
    public Set<Infections> getInfections() {
        return infections;
    }

    public void setInfections(Set<Infections> infections) {
        this.infections = infections;
    }

    @Override
    public String toString() {
        return "Tasks{" +
                "id=" + id +
                ", name=" + name +
                ", createdDate='" + createdDate + '\'' +
                ", completed=" + completed +
                ", command='" + command + '\'' +
                ", response='" + response + '\'' +
                ", infections=" + infections +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Tasks)) return false;
        Tasks tasks = (Tasks) o;
        return (getId() == tasks.getId() &&
                Objects.equals(getResponse(), tasks.getResponse())) ||
                (isCompleted() == tasks.isCompleted() &&
                        getName() == tasks.getName() &&
                        Objects.equals(getCommand(), tasks.getCommand()) &&
                        Objects.equals(getInfections(), tasks.getInfections()));
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), isCompleted(), getCommand(), getInfections());
    }
}
