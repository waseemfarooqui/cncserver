/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.air.cncserver.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Set;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 28-11-2018
 * <pattern>
 * The Model class is basically an Entity for the database that would used to store the geoloc information of the infected system.
 */

@Entity
@Table(name = "geo_locations")
public class GeoLocations implements Serializable {
    private Integer id;
    private String ipAddress;
    private String latitude;
    private String longitude;
    private String city;
    private String Country;
    private boolean iPrivate;

    private Set<Infections> infections;

    public GeoLocations() {
    }

    public GeoLocations(String ipAddress, String country, String city, String latitude, String longitude) {
        this.ipAddress = ipAddress;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        Country = country;
    }

    public GeoLocations(String ipAddress, String latitude, String longitude, String city, String country, boolean iPrivate) {
        this.ipAddress = ipAddress;
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        Country = country;
        this.iPrivate = iPrivate;
    }

    public GeoLocations(String country, String city, String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.city = city;
        Country = country;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @NotNull
    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    @NotNull
    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    @NotNull
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @NotNull
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @NotNull
    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public boolean isiPrivate() {
        return iPrivate;
    }

    public void setiPrivate(boolean iPrivate) {
        this.iPrivate = iPrivate;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "geo_location_infections", joinColumns = @JoinColumn(name = "geo_locations_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "infections_pc_signature_hash", referencedColumnName = "pcSignatureHash"))
    public Set<Infections> getInfections() {
        return infections;
    }

    public void setInfections(Set<Infections> infections) {
        this.infections = infections;
    }
}
