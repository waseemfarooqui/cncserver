/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.air.cncserver.models;

import javax.persistence.*;
import java.util.Collection;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 28-11-2018
 * <pattern>
 * The Model class is basically an Entity for the database that would used to store the AgentGroup Information.
 */
@Entity
@Table(name = "agent_group")
public class AgentGroup {

    private Long id;
    private String name;
    private Collection<Agents> agents;

    public AgentGroup() {
    }

    public AgentGroup(String name) {
        this.name = name;
    }

    public AgentGroup(String name, Collection<Agents> agents) {
        this.name = name;
        this.agents = agents;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @OneToMany(mappedBy = "agentGroup", cascade = CascadeType.ALL)
    public Collection<Agents> getAgents() {
        return agents;
    }

    public void setAgents(Collection<Agents> agents) {
        this.agents = agents;
    }
}
