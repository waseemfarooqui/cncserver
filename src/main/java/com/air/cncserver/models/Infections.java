/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.air.cncserver.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Set;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 28-11-2018
 * <pattern>
 * The Model class is basically an Entity for the database that would used to store the Infection.
 */
@Entity
@Table(name = "infections")
public class Infections {

    @NotNull
    private String infectedBy;
    @NotNull
    private String pcSignature;
    @NotNull
    private String pcSignatureHash;
    @NotNull
    private PCs pc;
    private Agents agent;
    private Set<Tasks> tasks;
    private Set<GeoLocations> geoLocations;

    public Infections() {
    }

    public Infections(String pcSignatureHash) {
        this.pcSignatureHash = pcSignatureHash;
    }

    public Infections(String infectedBy, String pcSignature, String pcSignatureHash, Agents agents) {
        this.infectedBy = infectedBy;
        this.pcSignature = pcSignature;
        this.pcSignatureHash = pcSignatureHash;
        this.agent = agents;
    }

    public Infections(String infectedBy, String pcSignature, String pcSignatureHash, PCs pc, Set<GeoLocations> geoLocations, Agents agent, Set<Tasks> tasks) {
        this.infectedBy = infectedBy;
        this.pcSignature = pcSignature;
        this.pcSignatureHash = pcSignatureHash;
        this.pc = pc;
        this.geoLocations = geoLocations;
        this.agent = agent;
        this.tasks = tasks;
    }

    public Infections(String infectedBy, String pcSignature, String pcSignatureHash, PCs pc, Agents agent) {
        this.infectedBy = infectedBy;
        this.pcSignature = pcSignature;
        this.pcSignatureHash = pcSignatureHash;
        this.pc = pc;
        this.agent = agent;
    }

    public Infections(String pcSignatureHash, Set<Tasks> tasks) {
        this.pcSignatureHash = pcSignatureHash;
        this.tasks = tasks;
    }

    public String getInfectedBy() {
        return infectedBy;
    }

    public void setInfectedBy(String infectedBy) {
        this.infectedBy = infectedBy;
    }

    @Column(unique = true, nullable = false)
    public String getPcSignature() {
        return pcSignature;
    }

    public void setPcSignature(String pcSignature) {
        this.pcSignature = pcSignature;
    }

    @Id
    public String getPcSignatureHash() {
        return pcSignatureHash;
    }

    public void setPcSignatureHash(String pcSignatureHash) {
        this.pcSignatureHash = pcSignatureHash;
    }

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "pcs_id")
    public PCs getPc() {
        return pc;
    }

    public void setPc(PCs pc) {
        this.pc = pc;
    }


    @ManyToMany(mappedBy = "infections")
    public Set<GeoLocations> getGeoLocations() {
        return geoLocations;
    }

    public void setGeoLocations(Set<GeoLocations> geoLocations) {
        this.geoLocations = geoLocations;
    }

    //
    @ManyToOne
    @JoinColumn(name = "agents_code", referencedColumnName = "user_code")
    public Agents getAgent() {
        return agent;
    }

    public void setAgent(Agents agent) {
        this.agent = agent;
    }

    @ManyToMany(mappedBy = "infections")
    public Set<Tasks> getTasks() {
        return tasks;
    }

    public void setTasks(Set<Tasks> tasks) {
        this.tasks = tasks;
    }
}
