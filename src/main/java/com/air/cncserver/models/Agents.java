/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.air.cncserver.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 28-11-2018
 * <pattern>
 * The Model class is basically an Entity for the database that would used to store the AgentInformation.
 */
@Entity
@Table(name = "agents")
public class Agents {
    private Long userCode;
    private String userName;
    private String password;
    private boolean enabled;
    private Role role;
    private AgentGroup agentGroup;
    private Set<Infections> infections;


    public Agents() {
    }

    public Agents(Long userCode) {
        this.userCode = userCode;
    }

    public Agents(Long userCode, String userName, String password, boolean enabled, Role role, AgentGroup agentGroup) {
        this.userCode = userCode;
        this.userName = userName;
        this.password = password;
        this.enabled = enabled;
        this.role = role;
        this.agentGroup = agentGroup;
    }

    @Id
    @Column(name = "user_code")
    public Long getUserCode() {
        return userCode;
    }

    public void setUserCode(Long userCode) {
        this.userCode = userCode;
    }

    @NotBlank
    @Column(nullable = false, updatable = false, unique = true)
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @NotBlank
    @Column(nullable = false)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(nullable = false, updatable = true)
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @ManyToOne
    @JoinColumn(name = "role_id", referencedColumnName = "id")
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @ManyToOne
    @JoinColumn(name = "agent_group_id", referencedColumnName = "id")
    public AgentGroup getAgentGroup() {
        return agentGroup;
    }

    public void setAgentGroup(AgentGroup agentGroup) {
        this.agentGroup = agentGroup;
    }

    @OneToMany(mappedBy = "agent", cascade = CascadeType.ALL, fetch = FetchType.EAGER, targetEntity = Infections.class)
    public Set<Infections> getInfections() {
        return infections;
    }

    public void setInfections(Set<Infections> infections) {
        this.infections = infections;
    }
}
