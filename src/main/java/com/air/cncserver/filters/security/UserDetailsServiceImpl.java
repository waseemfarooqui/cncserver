package com.air.cncserver.filters.security;

import com.air.cncserver.models.Agents;
import com.air.cncserver.repositories.AgentRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Called at the time of creation of new token on the login
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private AgentRepository agentRepository;
    private static final Logger logger = LogManager.getLogger(UserDetailsServiceImpl.class);


    public UserDetailsServiceImpl(AgentRepository agentRepository) {
        this.agentRepository = agentRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        logger.info("Loading the username and the password");
        Agents agents = agentRepository.findAgentsByUserName(username);
        if (agents == null) {
            throw new UsernameNotFoundException(username + " Not Exists");
        }
        logger.info(agents.getRole().getName());
        agents.setEnabled(true);
        agentRepository.save(agents);
        return new org.springframework.security.core.userdetails.User(agents.getUserName(), agents.getPassword(), agents.isEnabled(),
                true, true, true, getAuthorities(agents.getRole().getName()));
    }

    private Collection<? extends GrantedAuthority> getAuthorities(
            String role) {

        List<GrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority(role));
        return authorities;
    }
}

