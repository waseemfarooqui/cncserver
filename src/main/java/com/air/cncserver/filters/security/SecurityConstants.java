package com.air.cncserver.filters.security;

public class SecurityConstants {
    public static final String SECRET = "1nD1@N@t10n@Lc0Ngr&$$";
    static final long EXPIRATION_TIME = 864_000_00; // 1 days
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
