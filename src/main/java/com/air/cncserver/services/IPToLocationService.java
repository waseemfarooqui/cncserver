/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.air.cncserver.services;

import com.air.cncserver.configurations.GeoLocationProperties;
import com.air.cncserver.models.GeoLocations;
import com.air.cncserver.repositories.GeoLocServiceInterface;
import com.air.cncserver.repositories.GeoLocationRepository;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 30-11-2018
 * <pattern>
 * The service would help in identification of the IP address.
 */
@Service
public class IPToLocationService implements GeoLocServiceInterface {

    private static final Logger logger = LogManager.getLogger(IPToLocationService.class);

    private DatabaseReader databaseReader;
    private GeoLocationRepository geoLocationRepository;
    private GeoLocationProperties geoLocationProperties;

    public IPToLocationService(GeoLocationRepository geoLocationRepository, GeoLocationProperties geoLocationProperties) throws IOException {
        this.geoLocationRepository = geoLocationRepository;
        this.geoLocationProperties = geoLocationProperties;
        this.databaseReader = new DatabaseReader.Builder(new File(this.geoLocationProperties.getPath())).build();

    }


    /**
     * The function would take the ip address and return the location object.
     *
     * @param ip The IP address whose information needs to be stored.
     * @return The geolocation
     * @throws IOException     while reading the mmdb
     * @throws GeoIp2Exception geoip based exception
     */
    @Override
    public GeoLocations getLocation(String ip) throws IOException, GeoIp2Exception {
        logger.info("Identifying the location of [{}].", ip);
        if (ip.contains(":")) {
            return null;
        }
        GeoLocations geoLocations = geoLocationRepository.getByIpAddress(ip);
        if (geoLocations == null) {
            logger.info("Geolocation for [{}] not exists in your database.", ip);
            InetAddress ipAddress = InetAddress.getByName(ip);
            if (ipAddress.isSiteLocalAddress()) {
                logger.info("The [{}] is local/private ip setting the default data.", ip);
                return new GeoLocations(ip,
                        "India",
                        "Ludhiana",
                        "32.0541528",
                        "73.5451425",
                        true);
            } else {
                logger.info("The [{}] is public/static ip address setting the data of db.", ip);
                CityResponse response = databaseReader.city(ipAddress);
                return new GeoLocations(ip,
                        response.getCountry().getName(),
                        response.getCity().getName(),
                        response.getLocation().getLatitude().toString(),
                        response.getLocation().getLongitude().toString(),
                        false);
            }
        }
        logger.info("The [{}] is already in your database", ip);
        return geoLocations;

    }


}
