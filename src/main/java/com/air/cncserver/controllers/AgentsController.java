/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.air.cncserver.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 29-11-2018
 * <pattern>
 * The controller would help in managing the agents.
 */

@Controller
@RequestMapping(Constants.BASE_URL_INFORMATION_HANDLER)
public class AgentsController {


}
