/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.air.cncserver.controllers;

import com.air.cncserver.repositories.StorageServiceInterface;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 06-12-2018
 * <pattern>
 * The controller would help in uploading files on the server.
 */
@Controller
@RequestMapping(Constants.BASE_URL_FILE_UPLOADER)
public class FileUploadController {

    private static final Logger logger = LogManager.getLogger(FileUploadController.class);

    private final StorageServiceInterface storageServiceInterface;

    @Autowired
    public FileUploadController(StorageServiceInterface storageServiceInterface) {
        this.storageServiceInterface = storageServiceInterface;
    }

    /**
     * This function will help in uploading the file user wants to upload.
     *
     * @param file The file user wants to upload.
     * @param directory Where the file needs to be store.
     * @param signatureHash The unique signature identified by the pc.
     * @return The status message whether the file has been upload or not.
     */
    @RequestMapping(value = Constants.URL_UPLOAD_FILE, method = RequestMethod.POST)
    public @ResponseBody
    ResponseEntity<JsonNode> uploadFile(
            @RequestParam("file") MultipartFile file,
            @RequestParam("hash") String signatureHash,
            @RequestParam("directory") String directory,
            Authentication authentication) {
//        logger.info("Authentication is [{}] and user is [{}]", authentication.getAuthorities(), authentication.getName());
        String fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (Constants.ALL_EXTENSIONS_LIST.contains(fileExtension)) {
            return ResponseEntity.status(HttpStatus.OK).body(storageServiceInterface.store(file, signatureHash, directory));
        } else
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(JsonNodeFactory.instance.objectNode().put("error", "Sorry! But we are not entertaining this [" + fileExtension + "] file format."));
    }

}
