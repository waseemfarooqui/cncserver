package com.air.cncserver.enumerations;

public enum StatusTypes {
    ACTIVE,
    INACTIVE,
    HIBERNATE,
    SLEEP
}
