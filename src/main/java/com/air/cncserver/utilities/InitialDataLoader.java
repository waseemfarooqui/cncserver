/*
 * Copyright 2018, Indian National Congress, https://www.inc.in/en
 * <pattern>
 * This software is released under the terms of the
 * GNU LGPL license. See http://www.gnu.org/licenses/lgpl.html
 * for more information.
 * <pattern>
 * Usage:   java -jar CnCServer.jar
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.air.cncserver.utilities;

import com.air.cncserver.controllers.Constants;
import com.air.cncserver.models.AgentGroup;
import com.air.cncserver.models.Agents;
import com.air.cncserver.models.Role;
import com.air.cncserver.repositories.AgentRepository;
import com.air.cncserver.repositories.GroupRepository;
import com.air.cncserver.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

/**
 * @author Rahual Ghandhi
 * @version 0.0.1
 * @since 29-11-2018
 * <pattern>
 * The class will load the initial data add the initial users.
 */
@Component
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    boolean alreadySetup = false;
    private AgentRepository agentRepository;
    private RoleRepository roleRepository;
    private GroupRepository groupRepository;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public InitialDataLoader(AgentRepository agentRepository, RoleRepository roleRepository, GroupRepository groupRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.agentRepository = agentRepository;
        this.roleRepository = roleRepository;
        this.groupRepository = groupRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Transactional
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        if (alreadySetup)
            return;

        Role superAdminRole = createRoleIfNotFound(Constants.ROLE_SUPER_ADMIN);
        Role adminRole = createRoleIfNotFound(Constants.ROLE_ADMIN);
        Role agentRole = createRoleIfNotFound(Constants.ROLE_AGENT);
        AgentGroup adminGroup = createGroupIfNotFound("a");
        createGroupIfNotFound("s");
        createGroupIfNotFound("k");
        createGroupIfNotFound("o");
        AgentGroup testGroup = createGroupIfNotFound("t");

        Agents test = agentRepository.findAgentsByUserName("test");
        if (test == null) {
            Agents test_user = new Agents((long) 23, "test", bCryptPasswordEncoder.encode("test19@TEST"), true, agentRole, testGroup);
            agentRepository.save(test_user);
        }
        Agents test2 = agentRepository.findAgentsByUserName("test2");
        if (test == null) {
            Agents test_user = new Agents((long) 144, "test2", bCryptPasswordEncoder.encode("test219@TEST"), true, agentRole, testGroup);
            agentRepository.save(test_user);
        }
        alreadySetup = true;
    }

    private Role createRoleIfNotFound(
            String name) {

        Role role = roleRepository.findByName(name);
        if (role == null) {
            role = new Role(name);
            roleRepository.save(role);
        }
        return role;
    }

    private AgentGroup createGroupIfNotFound(
            String name) {

        AgentGroup userGroup = groupRepository.findByName(name);
        if (userGroup == null) {
            userGroup = new AgentGroup(name);
            groupRepository.save(userGroup);
        }
        return userGroup;
    }
}
